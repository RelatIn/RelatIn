from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .models import FriendList  
from .forms import Add_Form


response = {}
def index(request):
    response['title'] = 'Add Friend'
    html = 'addFriend.html'
    response['add_form'] = Add_Form
    response['friend'] = FriendList.objects.all()
    return render(request, html, response)

def add_new_friend(request):
    form = Add_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = FriendList(name=response['name'],url=response['url'])
        friend.save()
        return HttpResponseRedirect('/add-friend/')      