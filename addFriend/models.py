from django.db import models
from django.core.validators import URLValidator

class FriendList(models.Model):
	name = models.CharField(max_length=30)
	url = models.URLField()
	date = models.DateTimeField(auto_now_add=True)
