from django import forms

class Add_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan url',
    }
    name_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan judul...'
    }
    url_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan url...'
    }

    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label='Url',required=True, widget=forms.URLInput(attrs=url_attrs))

