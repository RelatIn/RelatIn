from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_new_friend
from .models import FriendList
from .forms import Add_Form
from django.utils import timezone

class addFriendUnitTest(TestCase):

    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code,200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_model_can_add_new_friend(self):
        #Adding a new friend
        new_friend = FriendList.objects.create(name='Rafie Dharmawan',url='http://beranda.ui.ac.id/personal/')

        #Retrieving all available friend
        counting_all_available_friend = FriendList.objects.all().count()
        self.assertEqual(counting_all_available_friend,1)

    def test_form_validation_for_blank_items(self):
        form = Add_Form(data={'name': '','url':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    def test_addfriend_post_success_and_render_the_result(self):
        test = 'Anonymous'
        url= 'http://ling-in.herokuapp.com/fitur-1/'
        response_post = Client().post('/add-friend/add_new_friend', {'name': test, 'url': url})
        self.assertEqual(response_post.status_code, 301)