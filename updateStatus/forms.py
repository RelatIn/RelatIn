from django import forms

class Status_Form(forms.Form):
    activity_attrs = {
        'type': 'text',
        'class': 'todo-form-textarea',
        'placeholder':'What happened today?'
    }

    activity = forms.CharField(label='', required= True, widget=forms.Textarea(attrs=activity_attrs))