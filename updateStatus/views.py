from django.shortcuts import render
from .models import Status
from django.http import HttpResponseRedirect
from .forms import Status_Form
from profilePage.models import Profile
# Create your views here.

response = {}
expert = ['Writing', 'Skimming', 'Drawing']
def index(request):
    try:
        profile = Profile.objects.get(pk = 1)
        response["Profile"] = profile
    except:
        profile = Profile(name="Fran Na Jaya",
                          birthday="25 April",
                          gender="Male",
                          expertise=expert,
                          description="Lion Ha-O~h! Lion Ha-O~h!",
                          email="fullbottles@gmail.com")
        profile.save()
        response["Profile"] = profile
    response['name'] = response['Profile'].name
    response['title'] = 'Update Status'
    status = Status.objects.all()
    response['status'] = status
    html = 'updateStatus.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['activity'] = request.POST['activity']
        status = Status(activity = response['activity'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')