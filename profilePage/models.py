from django.db import models

# Create your models here.

class Profile(models.Model):
   name = models.CharField(max_length= 30)
   birthday = models.TextField()
   gender = models.TextField()
   expertise = models.TextField()
   description = models.TextField()
   email = models.TextField()
