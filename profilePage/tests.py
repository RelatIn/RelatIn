from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, expert
from .models import Profile
import unittest

# Create your tests here.
class ProfilePageUnitTest(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
    
    

    #lolos jangan diganti
    def test_update_status_using_name_from_profile(self):
        profile = Profile(name="Fran aja",
                          birthday="25 April",
                          gender="Male",
                          expertise=["hehe"],
                          description="Lion Ha-O~h! Lion Ha-O~h!",
                          email="fullbottles@gmail.com")
        profile.save()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn("Fran aja", html_response)
