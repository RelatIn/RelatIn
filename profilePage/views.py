from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Profile
# Create your views here.

response = {}
expert = ["Writing Skimming Drawing"]
def index(request):

    try:
        profile = Profile.objects.get(pk = 1)
        response["Profile"] = profile
        

    except:
        profile = Profile(name="Fran Na Jaya",
                          birthday="25 April",
                          gender="Male",
                          expertise=expert,
                          description="Lion Ha-O~h! Lion Ha-O~h!",
                          email="fullbottles@gmail.com")
        profile.save()

        response["Profile"] = profile
        response["expertise"] = expert
        
    html = 'profilePage.html'    
    return render(request, html, response)