from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import FriendList, Status, Profile
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class DashboardUnitTest(TestCase):

    #lolos
    def test_hello_name_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code,200)

    #lolos
    def test_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    #lolos
    def test_friend_is_already_count(self):
        response = self.client.post('/add-friend/add_new_friend/', data={'name': 'Rafie Dharmawan', 'url' : 'http://www.rafie.com'})
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('UTF-8')
        self.assertIn(r'<b>Friend</b> : 1 person', html_response)

    #lolos
    def test_feed_is_already_count(self):
        response = self.client.post('/status/add_status/', data={'activity': 'Main bareng yuk'})
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('UTF-8')
        self.assertIn(r'<b>Feeds</b> : 1 feed', html_response)

    #lolos
    def test_last_feed_already_show(self):
        response = self.client.post('/status/add_status/', data={'activity': 'Selesain dah test kalian masing masing'})
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('UTF-8')
        self.assertIn(r'Selesain dah test kalian masing masing', html_response)

    def test_update_status_using_name_from_profile(self):
        profile = Profile(name="Fran aja",
                          birthday="25 April",
                          gender="Male",
                          expertise=["hehe"],
                          description="Lion Ha-O~h! Lion Ha-O~h!",
                          email="fullbottles@gmail.com")
        profile.save()
        response = Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn("Fran aja", html_response)
        
    ############################# SEMUA APPS HARUS SAMA ###################################

    def test_navigation_exist(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('UTF-8')
        self.assertIn(r'<a href="/status/">Status</a>',html_response)
        self.assertIn(r'<a href="/profile/">Profile</a>', html_response)
        self.assertIn(r'<a href="/add-friend/">Friend</a>', html_response)
        self.assertIn(r'<a href="/dashboard/">Stats</a>', html_response)



    def test_footer_exist(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('UTF-8')
        self.assertIn(r'<p>&copy; copyright 2017</p>', html_response)

    ############################# SEMUA APPS HARUS SAMA ###################################
# gagal test
# class Lab5FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
#         super(Lab5FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab5FunctionalTest, self).tearDown()

#     def test_there_is_my_name(self):
#         selenium = self.selenium
#         selenium.get('http://127.0.0.1:8000/status/')
#         activity = selenium.find_element_by_id('id_activity')
#         activity.send_keys('Mengerjakan Lab PPW')
#         submit = selenium.find_element_by_id('submit')
#         submit.send_keys(Keys.RETURN)
#         assert 'Mengerjakan Lab PPW' in selenium.page_source