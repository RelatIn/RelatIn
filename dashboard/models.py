from django.db import models
from addFriend.models import FriendList
from updateStatus.models import Status
from profilePage.models import Profile
# Create your models here.

class Theme(models.Model):
    name = models.CharField(max_length = 100)