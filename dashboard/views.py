from django.shortcuts import render
from .models import FriendList, Status, Profile
# how to get last data inserted

# Create your views here.
response = {}

expert = ['Writing', 'Skimming', 'Drawing']
def index(request):
    try:
        profile = Profile.objects.get(pk = 1)
        response["profile"] = profile


    except:
        profile = Profile(name="Fran Na Jaya",
                          birthday="25 April",
                          gender="Male",
                          expertise=expert,
                          description="Lion Ha-O~h! Lion Ha-O~h!",
                          email="fullbottles@gmail.com")
        profile.save()

        response["profile"] = profile
    myStatusDataResp = Status.objects.all().count()
    myFriendDataResp = FriendList.objects.all().count()
    response['title'] = 'Dashboard'
    response['friend'] = myFriendDataResp
    response['feed'] = myStatusDataResp
    response['lastFeed'] = Status.objects.all().last()
    html = 'dashboard.html'
    return render(request, html, response)